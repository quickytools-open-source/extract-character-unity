# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0.3] - 2022-03-25
### Changed
- Character assets may contain extra and/or overriding animations

## [2.0.1] - 2022-02-10
### Changed
- Upgraded Unity to 2021
### Fixed
- Do not extract frameless looping animation transitions

## [1.1.0] - 2021-09-21
### Added
- Complete biped animation controller

## [1.0.5] - 2021-08-05
### Added
- Transparent vertex color material

## [1.0.4] - 2021-08-03
### Added
- T pose prefab is generated
### Changed
- Character structure is simplified requiring less work on extraction
- Skip standard animation controller when standard animations are not extracted

## [1.0.2] - 2021-07-23
### Changed
- Added mesh shadow fill option

## [1.0.0] - 2021-07-19
### Changed
- Change name from Day6 to quickytools Character
- Auto extract folder is now Assets/quickytools/Character/AutoExtract

## [0.0.7] - 2021-06-17
### Added
- Material (standard or URP) is set for character colors.
- Prompt about unsaved scene changes before generating character scene.

## [0.0.6] - 2021-06-05
### Added
- Extracts characters into a prefab.
- Generates a demo scene.
- Auto extract assets placed in specific folder (Assets/quickytools/Day6/AutoExtract).