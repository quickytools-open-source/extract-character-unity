using UnityEngine;
using UnityEngine.InputSystem;

namespace Quickytools
{
    public class CharacterInputListener : MonoBehaviour
    {
        private void OnMove(InputValue value)
        {
            var inputVector = value.Get<Vector2>();
            CharacterInputEvents.MoveCharacterEvent?.Invoke(inputVector);
        }
    }
}