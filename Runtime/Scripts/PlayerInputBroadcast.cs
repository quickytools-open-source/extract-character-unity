using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputBroadcast : MonoBehaviour
{
    public static Action<Vector2> MoveEvent;
    public static Action<bool> JumpEvent;
    public static Action<Vector2> LookEvent;
    public static Action<float> ChangeSpeedEvent;

    private void OnMove(InputValue value)
    {
        MoveEvent?.Invoke(value.Get<Vector2>());
    }

    private void OnJump(InputValue value)
    {
        JumpEvent?.Invoke(value.isPressed);
    }

    private void OnLook(InputValue value)
    {
        LookEvent?.Invoke(value.Get<Vector2>());
    }

    private void OnChangeSpeed(InputValue value)
    {
        ChangeSpeedEvent?.Invoke(value.Get<float>());
    }
}
