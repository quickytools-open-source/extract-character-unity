using System;
using UnityEngine;

namespace Quickytools
{
    public static class CharacterInputEvents
    {
        public static Action<Vector2> MoveCharacterEvent;
    }
}