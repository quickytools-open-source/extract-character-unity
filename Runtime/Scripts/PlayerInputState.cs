using UnityEngine;

public class PlayerInputState : MonoBehaviour
{
    public Vector2 MoveInput
    {
        get;
        private set;
    }

    public bool JumpInput
    {
        get;
        private set;
    }

    public Vector2 LookInput
    {
        get;
        private set;
    }

    private void OnEnable()
    {
        PlayerInputBroadcast.MoveEvent += OnMoveInput;
        PlayerInputBroadcast.JumpEvent += OnJumpInput;
        PlayerInputBroadcast.LookEvent += OnLookInput;
    }

    private void OnDisable()
    {
        PlayerInputBroadcast.MoveEvent += OnMoveInput;
        PlayerInputBroadcast.JumpEvent += OnJumpInput;
        PlayerInputBroadcast.LookEvent += OnLookInput;
    }

    private void OnMoveInput(Vector2 moveInput)
    {
        MoveInput = moveInput;
    }

    private void OnJumpInput(bool jumpInput)
    {
        JumpInput = jumpInput;
    }

    private void OnLookInput(Vector2 lookInput)
    {
        LookInput = lookInput;
    }

    public bool TakeJump()
    {
        var b = JumpInput;
        JumpInput = false;
        return b;
    }
}
