using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public static class CharacterExtractUtil
{
    public const int StandardRenderingPipeline = 0;
    public const int UniversalRenderingPipeline = 1;
    public const int HdRenderingPipeline = 2;
    public static int RenderingPipeline
    {
        get
        {
            var rp = StandardRenderingPipeline;
            var renderingPipeline = GraphicsSettings.renderPipelineAsset?.GetType()?.ToString() ?? "";
            if (renderingPipeline.ToLower().Contains("universal"))
            {
                rp = UniversalRenderingPipeline;
            }
            // TODO HDRP material
            return rp;
        }
    }

    private static readonly IDictionary<string, string> AnimationClipNameOverrideMap = new Dictionary<string, string>
    {
        {"start_", "quickytools-start"},
        {"idle_loop_", "quickytools-idle"},
        {"jump-in-place_in_", "quickytools-jump-up"},
        {"jump-in-place_loop_", "quickytools-airborne"},
        {"jump-in-place_out_", "quickytools-land"},
        {"walk_loop_", "quickytools-walk"},
        {"jog_loop_", "quickytools-jog"},
        {"run_loop_", "quickytools-run"},
    };

    public static (Renderer, Transform) GetMeshRendererRootBone(GameObject characterObject)
    {
        SkinnedMeshRenderer meshRenderer = null;
        Transform rootBone = null;
        foreach (Transform child in characterObject.transform)
        {
            var renderer = child.GetComponent<SkinnedMeshRenderer>();
            if (renderer != null)
            {
                if (renderer.rootBone.name.ToLower().Contains("root"))
                {
                    break;
                }
                meshRenderer = renderer;
            }
            else if (child.childCount == 1)
            {
                var depth = 0;
                var transform = child;
                while (rootBone == null && transform != null && depth++ < 3)
                {
                    if (transform.name.ToLower().Contains("root"))
                    {
                        rootBone = transform;
                        break;
                    }
                    else
                    {
                        transform = transform.GetChild(0);
                    }
                }
            }
        }

        return (meshRenderer, rootBone);
    }

    public static void ConfigureRendererSkeleton(Renderer renderer, Transform rootBone)
    {
        // Helps with filling shadow seams https://forum.unity.com/threads/light-passing-through-mesh-at-seams.641449/
        renderer.shadowCastingMode = ShadowCastingMode.TwoSided;

        if (rootBone != null)
        {
            if (renderer is SkinnedMeshRenderer meshRenderer)
            {
                if (meshRenderer.rootBone.parent != rootBone && meshRenderer.rootBone != rootBone)
                {
                    Debug.LogWarning($"Root bone {meshRenderer.rootBone} requires assignment");
                    meshRenderer.rootBone = rootBone;
                }
            }
        }
    }

    private static BodySizes CrawlBodySize(Transform bone, BodySizes bodySizes)
    {
        if (bone.position.y > bodySizes.Height)
        {
            bodySizes.Height = bone.position.y;
        }
        var isShoulderBone = bone.name.StartsWith("shoulder_");
        if (isShoulderBone || bone.name.StartsWith("upper_leg_"))
        {
            var radiusBone = isShoulderBone ? bone.GetChild(0) : bone;
            var p = radiusBone.position;
            p.y = 0;
            var radius = p.magnitude;
            bodySizes.Radius = Mathf.Max(bodySizes.Radius, radius);
        }

        foreach (Transform child in bone)
        {
            bodySizes = CrawlBodySize(child, bodySizes);
        }
        return bodySizes;
    }

    public static BodySizes GetBodySizes(Transform rootBone)
    {
        return CrawlBodySize(rootBone, new BodySizes());
    }

    public static AnimatorOverrideController CreateAnimatorOverrideController(
        RuntimeAnimatorController baseAnimatorController,
        IEnumerable<AnimationClip> animations
    )
    {
        var overrideController = new AnimatorOverrideController
        {
            runtimeAnimatorController = baseAnimatorController
        };
        var overrideAnimationClips = new List<KeyValuePair<AnimationClip, AnimationClip>>();
        var overridingClips = new Dictionary<string, AnimationClip>();
        foreach (var animationClip in animations)
        {
            var nameLower = animationClip.name.ToLower();
            foreach (var matchingName in AnimationClipNameOverrideMap.Keys)
            {
                if (nameLower.StartsWith(matchingName))
                {
                    overridingClips[AnimationClipNameOverrideMap[matchingName]] = animationClip;
                    break;
                }
            }
        }
        foreach (var animationClip in overrideController.animationClips)
        {
            var clipName = animationClip.name.ToLower();
            if (overridingClips.TryGetValue(clipName, out var overrideClip))
            {
                overrideAnimationClips.Add(new KeyValuePair<AnimationClip, AnimationClip>(animationClip, overrideClip));
            }
            else
            {
                return null;
            }
        }
        overrideController.ApplyOverrides(overrideAnimationClips);
        return overrideController;
    }
}

public struct BodySizes
{
    public float Height;
    public float Radius;
}