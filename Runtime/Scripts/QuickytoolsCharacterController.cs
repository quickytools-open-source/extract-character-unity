using UnityEngine;

namespace Quickytools
{
    [RequireComponent(typeof(CharacterController))]
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(PlayerInputState))]
    public class QuickytoolsCharacterController : MonoBehaviour
    {
        public float maxForwardSpeed = 2f;
        public float gravity = 9.81f;
        public float jumpSpeed = 5f;
        public float rotationSmoothTime = 0.12f;

        private const float k_GroundedRayDistance = 1f;
        private const float k_StickingGravityProportion = 0.3f;
        private const float k_GroundAcceleration = 20f;
        private const float k_GroundDeceleration = 25f;

        private readonly int ForwardSpeedAnimatorHash = Animator.StringToHash("ForwardSpeed");
        private readonly int VerticalSpeedAnimatorHash = Animator.StringToHash("VerticalSpeed");
        private readonly int GroundedAnimatorHash = Animator.StringToHash("Grounded");
        private readonly int AirborneAnimatorHash = Animator.StringToHash("Airborne");

        protected bool isGrounded = true;
        protected bool isJumpPossible;
        protected float forwardSpeedTarget;
        protected float forwardSpeed;
        protected float verticalSpeed;

        protected CharacterController capsuleCharacter;
        protected Animator animator;
        protected PlayerInputState playerInput;

        protected bool IsMoveInput => playerInput.MoveInput != default;

        private RaycastHit[] raycastHits = new RaycastHit[1];

        private void OnEnable()
        {
            playerInput = GetComponent<PlayerInputState>();
            animator = GetComponent<Animator>();
            capsuleCharacter = GetComponent<CharacterController>();

            PlayerInputBroadcast.ChangeSpeedEvent += ChangeSpeed;
        }

        private void OnDisable()
        {
            PlayerInputBroadcast.ChangeSpeedEvent -= ChangeSpeed;
        }

        private void FixedUpdate()
        {
            CalculateForwardMovement();
            CalculateVerticalMovement();

            SetTargetRotation();
        }

        private void CalculateForwardMovement()
        {
            if (!isGrounded) { return; }

            var moveInput = playerInput.MoveInput;
            if (moveInput.sqrMagnitude > 1f)
            {
                moveInput.Normalize();
            }
            forwardSpeedTarget = moveInput.magnitude * maxForwardSpeed;

            float acceleration = IsMoveInput ? k_GroundAcceleration : k_GroundDeceleration;
            forwardSpeed = Mathf.MoveTowards(forwardSpeed, forwardSpeedTarget, acceleration * Time.deltaTime);

            animator.SetFloat(ForwardSpeedAnimatorHash, forwardSpeed);
        }

        private void CalculateVerticalMovement()
        {
            if (!playerInput.JumpInput && isGrounded)
            {
                isJumpPossible = true;
            }

            if (isGrounded)
            {
                // When grounded apply a slight negative vertical speed to keep grounded.
                verticalSpeed = -gravity * k_StickingGravityProportion;

                if (playerInput.TakeJump() && isJumpPossible)
                {
                    verticalSpeed = jumpSpeed;
                    isGrounded = false;
                    isJumpPossible = false;

                    animator.SetBool(AirborneAnimatorHash, true);
                }
            }
            else
            {
                if (Mathf.Approximately(verticalSpeed, 0f))
                {
                    verticalSpeed = 0f;
                }

                verticalSpeed -= gravity * Time.deltaTime;
            }
        }

        private void SetTargetRotation()
        {
            if (!(IsMoveInput && isGrounded))
            {
                return;
            }

            var moveInput = playerInput.MoveInput;
            var targetRotation = Mathf.Atan2(moveInput.x, moveInput.y) * Mathf.Rad2Deg + Camera.main.transform.eulerAngles.y;
            var rotationVelocity = 0f;
            var rotation = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetRotation, ref rotationVelocity, rotationSmoothTime);

            transform.rotation = Quaternion.Euler(0.0f, rotation, 0.0f);
        }

        // Override animator root motion with code
        private void OnAnimatorMove()
        {
            Vector3 movement;
            if (isGrounded)
            {
                var ray = new Ray(transform.position + Vector3.up * k_GroundedRayDistance * 0.5f, -Vector3.up);
                var hitCount = Physics.RaycastNonAlloc(ray, raycastHits, k_GroundedRayDistance, Physics.AllLayers, QueryTriggerInteraction.Ignore);
                if (hitCount > 0)
                {
                    // ... and get the movement of the root motion rotated to lie along the plane of the ground.
                    movement = Vector3.ProjectOnPlane(animator.deltaPosition, raycastHits[0].normal);
                }
                else
                {
                    // If no ground is hit just get the movement as the root motion.
                    // Theoretically this should rarely happen as when grounded the ray should always hit.
                    movement = animator.deltaPosition;
                }
            }
            else
            {
                // If not grounded the movement continues along player forward (no resistance)
                movement = Mathf.Approximately(0, forwardSpeed)
                    ? Vector3.zero
                    : forwardSpeed * transform.forward * Time.deltaTime;
            }

            // Sync rotation between capsule controller and animator
            capsuleCharacter.transform.rotation *= animator.deltaRotation;

            // Add vertical motion
            movement += verticalSpeed * Vector3.up * Time.deltaTime;

            // Move the capsule controller
            capsuleCharacter.Move(movement);

            // Update state after capsule controller is moved
            isGrounded = capsuleCharacter.isGrounded;

            // Sync grounded state with animator according to character controller
            if (isGrounded)
            {
                animator.SetBool(AirborneAnimatorHash, false);
            }
            else
            {
                animator.SetFloat(VerticalSpeedAnimatorHash, verticalSpeed);
            }
            animator.SetBool(GroundedAnimatorHash, isGrounded);
        }

        private void ChangeSpeed(float speedChange)
        {
            if (Mathf.Approximately(0, speedChange)) { return; }

            maxForwardSpeed = Mathf.Clamp(maxForwardSpeed + speedChange * 0.5f, 1, 5);
        }
    }
}