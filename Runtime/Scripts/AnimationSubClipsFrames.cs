using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class AnimationSubClipsFrames
{
    private static readonly Regex clipIdFramesRegex = new Regex("([a-z_]+)(\\d+)", RegexOptions.IgnoreCase);
    private static readonly Regex frameMarkersRegex = new Regex("(?:^|_)(\\d+)", RegexOptions.IgnoreCase);

    private static AnimationSubClipsFrames ConvertAnimationSubClip(
        string clipId,
        string clipDescription,
        List<int> frames
    )
    {
        var startFrame = frames[0];
        var endFrame = frames[1];
        var loopStart = 0;
        var loopCount = 0;
        if (frames.Count > 2)
        {
            loopStart = startFrame + frames[2] - 1;
            loopCount = frames[3];
        }

        var clip = new AnimationSubClipsFrames
        {
            ClipId = clipId,
            StartFrame = startFrame,
            EndFrame = endFrame,
            LoopStartFrame = loopStart,
            LoopEndFrame = loopStart + loopCount
        };

        if (clipDescription.Contains("_fms"))
        {
            var frameMarkerDescription = clipDescription.Split(new string[] { "_fms" }, 2, StringSplitOptions.None)[1];
            var frameMarkers = new List<int>();
            var fmMatches = frameMarkersRegex.Matches(frameMarkerDescription);
            foreach (Match match in fmMatches)
            {
                if (int.TryParse(match.Groups[1].Value, out int fm))
                {
                    frameMarkers.Add(startFrame + fm - 1);
                }
            }
            clip.FrameMarkers = frameMarkers;
        }

        return clip;
    }

    private static AnimationSubClipsFrames ConvertPoseSubClip(string clipId, int frame)
    {
        return new AnimationSubClipsFrames
        {
            ClipId = clipId,
            StartFrame = frame,
            EndFrame = frame + 1,
        };
    }

    public static IList<AnimationSubClipsFrames> ExtractFrames(string clipName)
    {
        var clips = new List<AnimationSubClipsFrames>();

        var splits = clipName?.Split('-') ?? new string[0];
        try
        {
            for (int i = 1; i < splits.Length; i++)
            {
                var clipId = "";
                var frames = new List<int>();
                var clipDescription = splits[i];
                var isExtra = clipDescription.StartsWith("a_") || clipDescription.StartsWith("p_");
                if (isExtra)
                {
                    var extraParts = clipDescription.Split("__");
                    clipId = extraParts[0];
                    var extraFrames = extraParts[1].Split("_");
                    foreach (var extraFrame in extraFrames)
                    {
                        if (int.TryParse(extraFrame, out int frame))
                        {
                            frames.Add(frame);
                        }
                    }
                }
                else
                {
                    var matches = clipIdFramesRegex.Matches(clipDescription);
                    foreach (Match match in matches)
                    {
                        if (int.TryParse(match.Groups[2].Value, out int frame))
                        {
                            frames.Add(frame);
                            if (string.IsNullOrEmpty(clipId))
                            {
                                clipId = match.Groups[1].Value.Trim('_');
                                if (string.IsNullOrWhiteSpace(clipId))
                                {
                                    clipId = "_";
                                }
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(clipId) && frames.Count > 0)
                {
                    var clip = frames.Count > 1
                        ? ConvertAnimationSubClip(clipId, clipDescription, frames)
                        : ConvertPoseSubClip(clipId, frames[0]);
                    clips.Add(clip);
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e);
            clips = new List<AnimationSubClipsFrames>();
        }

        return clips;
    }

    private static IDictionary<string, string> IdNameMap = new Dictionary<string, string>
    {
        { "st", "start" },
        { "is", "idle" },
        { "jm", "jump" },
        { "js", "jump-in-place" },
        { "wl", "walk" },
        { "jg", "jog" },
        { "rn", "run" },
    };

    public string ClipId = "";
    public string ClipName => IdNameMap.TryGetValue(ClipId, out var name) ? name : ClipId;
    public int StartFrame;
    public int EndFrame;
    public bool HasLoop => LoopEndFrame > LoopStartFrame;
    public int LoopStartFrame;
    public int LoopEndFrame;
    public bool HasFrameMarkers => FrameMarkers.Count > 0;
    // TODO Guard against setting null
    public IList<int> FrameMarkers = new List<int>();

    public override string ToString()
    {
        var s = $"{ClipId} {StartFrame}-{EndFrame}";
        if (HasLoop)
        {
            s += $" loops {LoopStartFrame}-{LoopEndFrame}";
        };
        if (FrameMarkers.Count > 0)
        {
            var joinedMarkers = string.Join(", ", FrameMarkers);
            s += $" frame markers {joinedMarkers}";
        }
        return s;
    }

    public override bool Equals(object obj)
    {
        return obj is AnimationSubClipsFrames frames &&
               ClipId == frames.ClipId &&
               StartFrame == frames.StartFrame &&
               EndFrame == frames.EndFrame &&
               LoopStartFrame == frames.LoopStartFrame &&
               LoopEndFrame == frames.LoopEndFrame &&
               IntegersEquals(FrameMarkers, frames.FrameMarkers);
        //    EqualityComparer<IList<int>>.Default.Equals(FrameMarkers, frames.FrameMarkers);
    }

    public override int GetHashCode()
    {
        HashCode hash = new HashCode();
        hash.Add(ClipId);
        hash.Add(StartFrame);
        hash.Add(EndFrame);
        hash.Add(LoopStartFrame);
        hash.Add(LoopEndFrame);
        hash.Add(FrameMarkers);
        return hash.ToHashCode();
    }

    // TODO Why doesn't generated Equals work for int lists?
    public static bool IntegersEquals(IList<int> l1, IList<int> l2)
    {
        var l1Count = l1?.Count ?? 0;
        var l2Count = l2?.Count ?? 0;
        if (l1Count == 0 && l2Count == 0) { return true; }
        for (var i = 0; i < l1Count; i++)
        {
            if (l1[i] != l2[i]) { return false; }
        }
        return true;
    }
}