using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.IO;
using UnityEditor.SceneManagement;

namespace Quickytools.Editor
{
    public class GenerateQuickytoolsCharacterScene : MonoBehaviour
    {
        [MenuItem("Tools/quickytools/Character/Generate character scene")]
        public static void GenerateScene()
        {
            var fbxAssetFiles = GetSelectedAssets();
            if (fbxAssetFiles.Count > 0)
            {
                if (!EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
                {
                    return;
                }
                // Other useful scene calls. See input samples/Bootstraps for additional calls.
                // EditorApplication.EnterPlaymode();
                // Select an object in the inspector
                // EditorGUIUtility.PingObject(MonoScript.FromMonoBehaviour(component));

                GenerateScene(fbxAssetFiles);
            }
        }

        private static IList<string> GetSelectedAssets()
        {
            var fbxAssetFiles = new List<string>();
            foreach (var obj in Selection.GetFiltered(typeof(Object), SelectionMode.Assets))
            {
                var path = AssetDatabase.GetAssetPath(obj);
                if (!string.IsNullOrEmpty(path) && File.Exists(path) && Path.GetExtension(path) == ".fbx")
                {
                    fbxAssetFiles.Add(path);
                }
            }

            if (fbxAssetFiles.Count == 0)
            {
                EditorUtility.DisplayDialog("Select files", "Select quickytools character asset files to import", "OK");
            }

            return fbxAssetFiles;
        }

        private const string MissingListenerMessage = "quickytools character input listener prefab not found.\nSomething was changed and this package was not updated.\nContact developers of this package to update it.";

        private static void GenerateScene(IList<string> assetPaths)
        {
            var inputListenerPrefabAssetPath = AssetUtil.GetFirstAssetPath($"QuickytoolsCharacterInput t:Prefab", MissingListenerMessage);
            if (string.IsNullOrEmpty(inputListenerPrefabAssetPath))
            {
                return;
            }

            foreach (var assetPath in assetPaths)
            {
                var (fileName, baseName, extractAssetPath) = PathUtil.GetExtractDirPath(assetPath);

                var characterPrefabAssetPath = AssetUtil.GetFirstAssetPath($"{baseName} t:Prefab", $"Character prefab for {baseName} not found.", $"{baseName}.prefab");
                if (string.IsNullOrEmpty(characterPrefabAssetPath))
                {
                    continue;
                }

                var scene = EditorSceneManager.NewScene(NewSceneSetup.DefaultGameObjects, NewSceneMode.Single);
                var prefabPaths = new string[] { characterPrefabAssetPath, inputListenerPrefabAssetPath };
                foreach (var prefabAssetPath in prefabPaths)
                {
                    var prefab = AssetDatabase.LoadAssetAtPath<GameObject>(prefabAssetPath);
                    Instantiate(prefab, new Vector3(0, 0, 0), Quaternion.identity);
                }

                // Ground plane
                var plane = GameObject.CreatePrimitive(PrimitiveType.Plane);
                plane.transform.localScale = new Vector3(5, 1, 5);

                EditorSceneManager.SaveScene(scene, $"{extractAssetPath}/{baseName}Scene.unity");
            }
        }
    }
}