using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Quickytools.Editor
{
    public class ProcessQuickytoolsCharacterImport : AssetPostprocessor
    {
        private static EditorApplication.CallbackFunction extractDelegate;
        private static IList<string> extractAssetPaths;

        private static void AddImportCharacterAssets(IEnumerable<string> assetPaths, IList<string> acc)
        {
            foreach (var assetPath in assetPaths)
            {
                // Perform filtering here since entire package can call this method with any paths
                var (dirPath, fileName, baseName) = PathUtil.GetFilePathName(assetPath);
                if (dirPath == PathUtil.CharacterImportRootDir && fileName.EndsWith(".fbx"))
                {
                    acc.Add(assetPath);
                }
            }
        }

        static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            PostProcessFbxAssets(importedAssets, movedAssets);
        }

        private static void PostProcessFbxAssets(string[] importedAssets, string[] movedAssets)
        {
            var importPaths = new List<string>();
            AddImportCharacterAssets(importedAssets, importPaths);
            AddImportCharacterAssets(movedAssets, importPaths);
            if (importPaths.Count == 0)
            {
                return;
            }

            var extractPaths = new List<string>();
            foreach (var importAssetPath in importPaths)
            {
                var (fileName, baseName, extractAssetPath) = PathUtil.GetExtractDirPath(importAssetPath);
                var extractAssetAbsPath = PathUtil.GetAbsPath(extractAssetPath);
                if (!Directory.Exists(extractAssetAbsPath))
                {
                    extractPaths.Add(importAssetPath);
                }
            }

            if (extractPaths.Count == 0)
            {
                return;
            }

            // AssetDatabase.Refresh() won't work from inside OnPostprocessAllAssets because it's a recursive call. Run it in a callback not originating from OnPostprocessAllAssets.
            // https://answers.unity.com/questions/647615/how-to-update-import-settings-for-newly-created-as.html
            if (extractDelegate == null)
            {
                extractAssetPaths = extractPaths;
                extractDelegate = new EditorApplication.CallbackFunction(AutoExtract);
                EditorApplication.update = Delegate.Combine(EditorApplication.update, extractDelegate) as EditorApplication.CallbackFunction;
            }
            else
            {
                Debug.Log("Postprocess triggered during extraction");
                return;
            }
        }

        private static void AutoExtract()
        {
            EditorApplication.update = Delegate.Remove(EditorApplication.update, extractDelegate) as EditorApplication.CallbackFunction;
            extractDelegate = null;

            var extractPaths = extractAssetPaths;
            extractAssetPaths = null;

            ExtractQuickytoolsCharacter.ExtractCharacterAssets(extractPaths, true);
        }
    }
}