using UnityEditor;

namespace Quickytools.Editor
{
    public static class AssetUtil
    {
        public static string GetFirstAssetPath(string query, string notFoundMessage = "", string exactFileName = "")
        {
            string assetPath = "";

            var animatorControllerGuids = AssetDatabase.FindAssets(query);
            if (animatorControllerGuids.Length == 0)
            {
                if (!string.IsNullOrEmpty(notFoundMessage))
                {
                    EditorUtility.DisplayDialog("File not found", notFoundMessage, "OK");
                }
            }
            else
            {
                var guid = animatorControllerGuids[0];
                assetPath = AssetDatabase.GUIDToAssetPath(guid);

                if (!string.IsNullOrEmpty(exactFileName))
                {
                    for (var i = 1; i < animatorControllerGuids.Length; i++)
                    {
                        guid = animatorControllerGuids[i];
                        var pathI = AssetDatabase.GUIDToAssetPath(guid);
                        if (pathI.EndsWith($"/{exactFileName}"))
                        {
                            assetPath = pathI;
                            break;
                        }
                    }
                }
            }

            return assetPath;
        }
    }
}