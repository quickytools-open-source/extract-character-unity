using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public static class AnimationUtil
{
    private static readonly ISet<string> LoopingAnimationsNames = new HashSet<string> { "idle", "walk" };

    private static IList<AnimationClip> GetAssetAnimations(string assetPath)
    {
        // Reference animation clips
        var subAssets = AssetDatabase.LoadAllAssetRepresentationsAtPath(assetPath);
        var animations = new List<AnimationClip>();
        foreach (Object o in subAssets)
        {
            if (o is AnimationClip clip)
            {
                animations.Add(clip);
            }
        }
        return animations;
    }

    private static bool IsLoopingAnimation(AnimationClip clip)
    {
        var name = clip.name.ToLower();
        foreach (var loopingName in LoopingAnimationsNames)
        {
            if (name.Contains(loopingName))
            {
                return true;
            }
        }
        return false;
    }

    private static AnimationClip CopyAnimation(AnimationClip animation)
    {
        var animationCopy = new AnimationClip();
        EditorUtility.CopySerialized(animation, animationCopy);
        if (IsLoopingAnimation(animation))
        {
            var settings = AnimationUtility.GetAnimationClipSettings(animationCopy);
            settings.loopTime = true;
            settings.loopBlend = true;
            AnimationUtility.SetAnimationClipSettings(animationCopy, settings);
        }
        return animationCopy;
    }

    private static (AnimationClip, AnimationClip, IList<AnimationClip>) CopyAnimations(
        IEnumerable<AnimationClip> originalAnimations, string secondaryAssetPath)
    {
        var copyAnimations = new List<AnimationClip>();
        AnimationClip startAnimationClip = null;
        AnimationClip entireAnimationClip = null;
        foreach (var animation in originalAnimations)
        {
            var animationCopy = CopyAnimation(animation);
            var animationNameLower = animationCopy.name.ToLower();
            if (animationNameLower.StartsWith("entire-"))
            {
                entireAnimationClip = animationCopy;
                break;
            }

            AssetDatabase.CreateAsset(animationCopy, $"{secondaryAssetPath}/{animation.name}.anim");
            if (startAnimationClip == null && animationNameLower.StartsWith("start_"))
            {
                startAnimationClip = animationCopy;
            }
            else
            {
                copyAnimations.Add(animationCopy);
            }
        }

        return (entireAnimationClip, startAnimationClip, copyAnimations);
    }

    public static (AnimationClip, IList<AnimationClip>) ExtractAnimations(
        ModelImporter importer,
        string baseName,
        string assetPath,
        string secondaryAssetPath)
    {
        var originalAnimations = GetAssetAnimations(assetPath);

        var (entireAnimationClip, startAnimationClip, copyAnimations) = CopyAnimations(originalAnimations, secondaryAssetPath);
        if (entireAnimationClip != null)
        {
            SplitAnimations(importer, entireAnimationClip, baseName, true);
            originalAnimations = GetAssetAnimations(assetPath);
            (_, startAnimationClip, copyAnimations) = CopyAnimations(originalAnimations, secondaryAssetPath);
        }
        return (startAnimationClip, copyAnimations);
    }

    private static void SplitAnimations(
        ModelImporter importer,
        AnimationClip entireAnimationClip,
        string baseName,
        bool saveTransitionSubClips)
    {
        var clipAnimations = new List<ModelImporterClipAnimation>();
        if (entireAnimationClip != null)
        {
            var subClipsFrames = AnimationSubClipsFrames.ExtractFrames(entireAnimationClip.name);
            foreach (var subClip in subClipsFrames)
            {
                var isIdle = subClip.ClipId == "is";
                var isJumpStationary = subClip.ClipId == "js";

                ModelImporterClipAnimation outClip = null;
                if (subClip.HasLoop)
                {
                    if (saveTransitionSubClips && subClip.LoopStartFrame - subClip.StartFrame > 0)
                    {
                        clipAnimations.Add(new ModelImporterClipAnimation
                        {
                            firstFrame = subClip.StartFrame,
                            lastFrame = subClip.LoopStartFrame,
                            name = $"{subClip.ClipName}_in_{baseName}",
                            lockRootPositionXZ = isIdle,
                            keepOriginalPositionXZ = isIdle,
                            keepOriginalPositionY = true
                        });
                    }
                    clipAnimations.Add(new ModelImporterClipAnimation
                    {
                        firstFrame = subClip.LoopStartFrame,
                        lastFrame = subClip.LoopEndFrame,
                        name = $"{subClip.ClipName}_loop_{baseName}",
                        lockRootPositionXZ = isIdle,
                        keepOriginalPositionXZ = isIdle,
                        keepOriginalPositionY = true,
                        loopTime = true,
                        loopPose = true
                    });
                    if (saveTransitionSubClips && subClip.EndFrame - subClip.LoopEndFrame > 1)
                    {
                        outClip = new ModelImporterClipAnimation
                        {
                            firstFrame = subClip.LoopEndFrame,
                            lastFrame = subClip.EndFrame,
                            name = $"{subClip.ClipName}_out_{baseName}",
                            lockRootPositionXZ = isIdle,
                            keepOriginalPositionXZ = isIdle,
                            keepOriginalPositionY = true
                        };
                    }
                }
                else
                {
                    clipAnimations.Add(new ModelImporterClipAnimation
                    {
                        firstFrame = subClip.StartFrame,
                        lastFrame = subClip.EndFrame,
                        name = $"{subClip.ClipName}_{baseName}",
                        keepOriginalPositionXZ = isIdle,
                        lockRootPositionXZ = isIdle,
                        keepOriginalPositionY = true
                    });
                }

                if (subClip.HasFrameMarkers)
                {
                    var startFrame = subClip.HasLoop ? subClip.LoopEndFrame : subClip.StartFrame;
                    int endFrame = 0;
                    var f = 0;
                    var padCount = Mathf.CeilToInt(Mathf.Log10(subClip.FrameMarkers.Count)) + 1;
                    foreach (var frameMarker in subClip.FrameMarkers)
                    {
                        endFrame = frameMarker - 1;
                        var indexPadded = f.ToString().PadLeft(padCount);
                        clipAnimations.Add(new ModelImporterClipAnimation
                        {
                            firstFrame = startFrame,
                            lastFrame = endFrame,
                            name = $"{subClip.ClipName}_{indexPadded}_{baseName}",
                            lockRootPositionXZ = isIdle,
                            keepOriginalPositionXZ = isIdle,
                            keepOriginalPositionY = true
                        });
                        startFrame = frameMarker;
                        f++;
                    }
                    outClip = new ModelImporterClipAnimation
                    {
                        // Jump in place tries to transition at the frame marker so start one after
                        firstFrame = endFrame + (isJumpStationary ? 1 : 0),
                        lastFrame = subClip.EndFrame,
                        name = $"{subClip.ClipName}_out_{baseName}",
                        lockRootPositionXZ = isIdle || isJumpStationary,
                        keepOriginalPositionXZ = isIdle || isJumpStationary,
                        lockRootHeightY = isJumpStationary,
                        keepOriginalPositionY = true
                    };
                }

                if (outClip != null)
                {
                    clipAnimations.Add(outClip);
                }
            }

            importer.clipAnimations = clipAnimations.ToArray();
            importer.SaveAndReimport();
        }
    }
}
