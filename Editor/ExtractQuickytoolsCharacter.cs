using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Quickytools.Editor
{
    public class ExtractQuickytoolsCharacter : MonoBehaviour
    {
        [MenuItem("Tools/quickytools/Character/Extract character")]
        public static void ExtractCharacter()
        {
            ExtractCharacter(false);
        }

        [MenuItem("Tools/quickytools/Character/Extract character - no prompts")]
        public static void ExtractCharacterNoPrompt()
        {
            ExtractCharacter(true);
        }

        private static void ExtractCharacter(bool force)
        {
            var fbxAssetFiles = GetSelectedAssets();
            if (fbxAssetFiles.Count > 0)
            {
                ExtractCharacterAssets(fbxAssetFiles, force);
            }
        }

        private static IList<string> GetSelectedAssets()
        {
            var fbxAssetFiles = new List<string>();
            foreach (var obj in Selection.GetFiltered(typeof(Object), SelectionMode.Assets))
            {
                var path = AssetDatabase.GetAssetPath(obj);
                if (!string.IsNullOrEmpty(path) && File.Exists(path) && Path.GetExtension(path) == ".fbx")
                {
                    fbxAssetFiles.Add(path);
                }
            }

            if (fbxAssetFiles.Count == 0)
            {
                EditorUtility.DisplayDialog("Select files", "Select quickytools character asset files to import", "OK");
            }

            return fbxAssetFiles;
        }

        private static (string, string, string, string) SetDirectoryStructure(string assetPath, bool force)
        {
            var (fileName, baseName, extractAssetPath) = PathUtil.GetExtractDirPath(assetPath);
            var extractDirAbsPath = $"{Application.dataPath}/{extractAssetPath}".Replace("Assets/Assets", "Assets");

            var secondaryDirName = "base-assets";
            var secondaryAssetPath = $"{extractAssetPath}/{secondaryDirName}";

            if (Directory.Exists(extractDirAbsPath) && !force)
            {
                var message = $"Contents in directory {extractAssetPath} will be overwritten.\nIf changes were made that need preserving press Cancel and move contents then extract again.";
                var overwrite = EditorUtility.DisplayDialog("Overwrite", message, "Continue", "Cancel");
                if (!overwrite)
                {
                    return ("", "", "", "");
                }
            }

            var dirAbsPaths = new string[] { extractDirAbsPath, $"{extractDirAbsPath}/{secondaryDirName}" };
            foreach (var dirAbsPath in dirAbsPaths)
            {
                if (!Directory.Exists(dirAbsPath))
                {
                    Directory.CreateDirectory(dirAbsPath);
                }
            }

            return (fileName, baseName, extractAssetPath, secondaryAssetPath);
        }

        internal static void ExtractCharacterAssets(IList<string> assetPaths, bool force = false)
        {
            foreach (var assetPath in assetPaths)
            {
                ExtractCharacter(assetPath, force);
            }
        }

        private static string CopyAssetForImport(string assetPath, string extractAssetPath, string baseName)
        {
            var copyAssetPath = $"{extractAssetPath}/{baseName}-import.fbx";
            AssetDatabase.CopyAsset(assetPath, copyAssetPath);
            return copyAssetPath;
        }

        private static void ExtractCharacter(string assetPath, bool force = false)
        {
            // Set directory structure
            var (fileName, baseName, extractAssetPath, secondaryAssetPath) = SetDirectoryStructure(assetPath, force);
            if (string.IsNullOrEmpty(fileName))
            {
                return;
            }

            assetPath = CopyAssetForImport(assetPath, extractAssetPath, baseName);
            var importer = ModelImporter.GetAtPath(assetPath) as ModelImporter;
            // Convert Units
            importer.useFileScale = false;

            // Humanoid animation type
            importer.animationType = ModelImporterAnimationType.Human;

            // Apply changes
            importer.SaveAndReimport();

            // Extract animation clips
            var (startAnimationClip, copyAnimations) = AnimationUtil.ExtractAnimations(importer, baseName, assetPath, secondaryAssetPath);

            // Create copy without animation clips
            var assetCopyPath = $"{secondaryAssetPath}/{fileName}";
            AssetDatabase.CopyAsset(assetPath, assetCopyPath);
            importer = ModelImporter.GetAtPath(assetCopyPath) as ModelImporter;
            importer.importAnimation = false;
            importer.SaveAndReimport();

            // Create minimal character prefab
            SaveCharacterPrefab(assetCopyPath, baseName, extractAssetPath, copyAnimations, secondaryAssetPath);

            if (startAnimationClip != null)
            {
                SaveCharacterPrefab(
                    assetCopyPath,
                    $"t-{baseName}",
                    extractAssetPath,
                    new AnimationClip[] { startAnimationClip },
                    secondaryAssetPath,
                    "QuickytoolsBipedTposeAnimationController",
                    false
                );
            }

            AssetDatabase.DeleteAsset(assetPath);

            AssetDatabase.SaveAssets();
        }

        private static void SaveCharacterPrefab(
            string assetCopyPath,
            string characterAssetName,
            string extractAssetPath,
            IEnumerable<AnimationClip> copyAnimationClips,
            string secondaryAssetPath,
            string animationControllerName = "",
            bool configureLocomotion = true
        )
        {
            var mainAsset = AssetDatabase.LoadMainAssetAtPath(assetCopyPath);
            var characterObject = Instantiate(mainAsset, Vector3.zero, Quaternion.identity) as GameObject;
            characterObject.name = characterAssetName;

            var (meshRenderer, rootBone) = CharacterExtractUtil.GetMeshRendererRootBone(characterObject);
            if (meshRenderer != null)
            {
                CharacterExtractUtil.ConfigureRendererSkeleton(meshRenderer, rootBone);
                SetMeshMaterial(meshRenderer);
            }

            var animatorControllerAssetPath = PathUtil.GetAnimationControllerPath(animationControllerName);
            if (copyAnimationClips.Count() > 0)
            {
                SetPrefabAnimatorController(
                    animatorControllerAssetPath,
                    copyAnimationClips,
                    secondaryAssetPath,
                    characterObject.name,
                    characterObject
                );
            }

            if (configureLocomotion)
            {
                var animator = characterObject.GetComponent<Animator>();
                animator.updateMode = AnimatorUpdateMode.AnimatePhysics;

                var bodySizes = CharacterExtractUtil.GetBodySizes(rootBone);

                var characterControl = characterObject.AddComponent<QuickytoolsCharacterController>();
                characterControl.jumpSpeed = bodySizes.Height * 2.5f;

                var capsuleController = characterObject.GetComponent<CharacterController>();
                capsuleController.skinWidth = 0.01f;
                var height = bodySizes.Height;
                capsuleController.height = height;
                capsuleController.center = new Vector3(0, height * 0.5f, 0);
                capsuleController.radius = bodySizes.Radius;
            }

            SaveCharacterPrefab(extractAssetPath, characterObject);

            DestroyImmediate(characterObject.gameObject);
        }

        private static bool SetPrefabAnimatorController(
            string animatorControllerAssetPath,
            IEnumerable<AnimationClip> copyAnimations,
            string secondaryAssetPath,
            string controllerAssetName,
            GameObject characterObject)
        {
            // Create animator override controller
            var baseAnimatorController = AssetDatabase.LoadAssetAtPath<RuntimeAnimatorController>(animatorControllerAssetPath);
            var overrideController = CharacterExtractUtil.CreateAnimatorOverrideController(baseAnimatorController, copyAnimations);
            if (overrideController == null)
            {
                EditorUtility.DisplayDialog(
                    "Incomplete extraction",
                    $"Animator override controller of {animatorControllerAssetPath} not created. Issue during extraction.", "OK"
                );
                return false;
            }

            var overrideControllerAssetPath = $"{secondaryAssetPath}/{controllerAssetName}.overrideController";
            AssetDatabase.CreateAsset(overrideController, overrideControllerAssetPath);
            AssetDatabase.SaveAssets();

            var animatorController = AssetDatabase.LoadAssetAtPath<RuntimeAnimatorController>(overrideControllerAssetPath);
            characterObject.GetComponent<Animator>().runtimeAnimatorController = animatorController;
            return true;
        }

        private static void SetMeshMaterial(Renderer renderer)
        {
            var materialName = "QuickytoolsVertexColorStandardMaterial";
            if (CharacterExtractUtil.RenderingPipeline != CharacterExtractUtil.StandardRenderingPipeline)
            {
                // TODO HDRP
                materialName = "QuickytoolsVertexColorUrpMaterial";
            }
            var materialAssetPath = AssetUtil.GetFirstAssetPath($"{materialName} t:Material");
            if (!string.IsNullOrEmpty(materialAssetPath))
            {
                var material = AssetDatabase.LoadAssetAtPath<Material>(materialAssetPath);
                renderer.material = material;
            }
        }

        private static string SaveCharacterPrefab(string extractAssetPath, GameObject characterObject)
        {
            var characterPrefabPath = $"{extractAssetPath}/{characterObject.name}.prefab";
            PrefabUtility.SaveAsPrefabAsset(characterObject, characterPrefabPath);
            return characterPrefabPath;
        }
    }
}