# Character Extract

Extracts quickytools Characters into prefabs for use in Unity. A sample scene can also be generated with the character prefab.

Package git URL is `https://gitlab.com/quickytools-open-source/extract-character-unity.git`.

Create a directory at Assets/quickytools/Character/AutoExtract and drag character assets into it to run extraction automatically.